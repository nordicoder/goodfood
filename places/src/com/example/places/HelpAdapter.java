package com.example.places;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HelpAdapter extends BaseAdapter {

	Context context;
	String[] help_items_disL;
	String[] help_items_disS;
	String[] help_items_title;
	public HelpAdapter(Context paramContext) {
		this.context = paramContext;
		this.help_items_title = paramContext.getResources().getStringArray(
				R.array.help_mainTitle);
		this.help_items_disL = paramContext.getResources().getStringArray(
				R.array.help_disL);
		this.help_items_disS = paramContext.getResources().getStringArray(
				R.array.help_disS);
	}

	public int getCount() {
		return this.help_items_title.length;
	}

	public Object getItem(int paramInt) {
		return this.help_items_title[paramInt];
	}

	public long getItemId(int paramInt) {
		return paramInt;
	}

	private class ViewHolder {
		TextView localTextView1 = null;
		TextView localTextView2 = null;
		TextView localTextView3 = null;
		TextView localTextView4 = null;
	}

	@Override
	public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
		// TODO Auto-generated method stub

		View localView;
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		String str;
		if (paramView == null) {
			paramView = mInflater.inflate(R.layout.help_items, null);
			holder = new ViewHolder();

			holder.localTextView1 = (TextView) paramView
					.findViewById(R.id.help_MainTitle);
			holder.localTextView2 = (TextView) paramView
					.findViewById(R.id.help_DiscrpL);
			holder.localTextView3 = (TextView) paramView
					.findViewById(R.id.help_DiscrpS);
			holder.localTextView4 = (TextView) paramView
					.findViewById(R.id.help_seperator);

			paramView.setTag(holder);
		} else {
			holder = (ViewHolder) paramView.getTag();
		}
		holder.localTextView1.setTextColor(context.getResources().getColor(
				R.color.Orange));
		holder.localTextView4.setBackgroundColor(context.getResources()
				.getColor(R.color.Orange));

		holder.localTextView1.setText(this.help_items_title[paramInt]);
		holder.localTextView2.setText(this.help_items_disL[paramInt]);
		holder.localTextView3.setText(this.help_items_disS[paramInt]);
		localView = paramView;
		return localView;

	}
}