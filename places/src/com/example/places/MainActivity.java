/*
For google api key:-
login to your mail account
go to https://code.google.com/apis/console/
goto services tab
click on places api and google map api button. Fill the details
goto api access tab and copy your google api key
 */

package com.example.places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity implements LocationListener {
	
	//*********************************MULTIPLE API KEYS SINCE GOOGLE ALLOWS ONLY 200 SEARCHED PER DAY***********************************

	private String googleAPIKey2 = "AIzaSyABknFwtqt5DdvLV9vxi6miEx6FrZmMA8k";
	private String googleAPIKey1 = "AIzaSyC79Mt0Z02yLoFM04tMlieSFCfhwhHm2i0";
	private String googleAPIKey = "AIzaSyAPDWD8S9kqx4JP1DLSJKWzneZSUIpKPnU";
	private boolean gps_enabled, network_enabled;

	Intent set;
	SharedPreferences sharedPref;
	Editor e;
	HttpClient client;
	HttpResponse res;
	HttpPost req;
	InputStream in;
	JSONObject jsonobj;
	JSONArray resarray;
	String requesturl, ans = "", addresst;
	HttpEntity jsonentity;
	AsyncTask<Void, Void, String> shareRegidTask;
	private static double lat, longi;
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 5 * 1;
	protected LocationManager locationManager;
	protected Context context;
	List<Places> listplaces = new ArrayList<Places>();
	Places plc;
	Bundle b;
	int alreadyrunningtask;
	private ActionBarDrawerToggle mDrawerToggle;

	Dialog dialog;
	ImageButton db1, db2;

	String[] menutitles;
	TypedArray menuIcons;
	private DrawerLayout mDrawerLayout;
	ListView mDrawerList;
	private List<Rowitem1> rowItems;
	private CustomAdapter1 adapter1;
	WindowManager wm;
	int width1;

	JSONObject locs, openhrs;
	JSONArray photos;
	String photo_reference, rating, opennow = "";
	ListView llv;
	CustomAdapter adapter;
	List<Imagedownload> imd = new ArrayList<Imagedownload>();
	Imagedownload im;
	int kk = 0;
	Bitmap bitmap;
	String results;
	String searchtype = "textsearch/json?";
	String syntax = "https://maps.googleapis.com/maps/api/place/";
	String types = "&types;=food";
	String query = "";
	String name = "&name=";
	String keyword = "&keyword=";
	String key = "&key=";
	String location = "&location=";
	String radius = "";
	ProgressBar pb;
	ActionBar bar;
	RelativeLayout layout;
	Location Loca;
	AdView adView;
View rootView;

	final String TAG = getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		layout = (RelativeLayout) findViewById(R.id.rl);
		bar = getActionBar();
		alreadyrunningtask = 0;
		handleIntent(getIntent());
		bar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#FF000000")));
		// bar.setTitle("RestaurantSniffer");

		//*********************************GET LOCATION************************************
		
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.setVisibility(View.VISIBLE);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		gps_enabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		network_enabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		if (network_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
			Log.d("@@@@@","praying to be here");
			if (locationManager != null)
            {
				Log.d("@@@@@","dangerous circumstances");

            Loca = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            onLocationChanged(Loca);
            }

		} else if (gps_enabled) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
		}
		
		else if (!gps_enabled && !network_enabled)
         {
             // no network provider is enabled
			Log.d("######3","i am finished");
         }

		//*********************************SET DRAWER LAYOUT************************************
		menutitles = getResources().getStringArray(R.array.titles);
		menuIcons = getResources().obtainTypedArray(R.array.icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.slider_list);

		rowItems = new ArrayList<Rowitem1>();

		for (int j = 0; j < menutitles.length; j++) {
			Rowitem1 items = new Rowitem1(menutitles[j],
					menuIcons.getResourceId(j, -1));
			rowItems.add(items);
		}

		menuIcons.recycle();

		adapter1 = new CustomAdapter1(getApplicationContext(), rowItems);

		mDrawerList.setAdapter(adapter1);

		bar.setDisplayHomeAsUpEnabled(true);
		bar.setHomeButtonEnabled(true);

		int width = getResources().getDisplayMetrics().widthPixels / 2;
		DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList
				.getLayoutParams();
		params.width = width;
		mDrawerList.setLayoutParams(params);

		class SlideitemListener implements ListView.OnItemClickListener {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// llv.setClickable(true);
				//
				// llv.setFocusable(true);
				// mDrawerLayout.setFocusable(false);
				// mDrawerList.setFocusable(false);
				// llv.setClickable(true);
				// Log.d("333333333","-------");

				switch (position) {
				case 0:
					Log.d("!!!!", "1111");
					mDrawerList.setItemChecked(position, true);
					mDrawerList.setSelection(position);

					set = new Intent(getApplicationContext(), Filters.class);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
					set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(set);
					finish();

					mDrawerLayout.closeDrawer(mDrawerList);
					break;
				case 1:
					Log.d("!!!!", "1111");
					mDrawerList.setItemChecked(position, true);
					mDrawerList.setSelection(position);
					set = new Intent(getApplicationContext(),
							MainActivity.class);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
					set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(set);
					finish();
					mDrawerLayout.closeDrawer(mDrawerList);
					break;

				case 2:
					Collections.sort(listplaces,
							Collections.reverseOrder(new Comparator<Places>() {
								@Override
								public int compare(Places pl1, Places pl2) {

									return pl1.ratings.compareTo(pl2.ratings);
								}
							}));
					adapter.notifyDataSetChanged();
					mDrawerLayout.closeDrawer(mDrawerList);

					break;

				case 4:

					Intent set = new Intent(getApplicationContext(),
							HelpActivity.class);
					set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(set);
					MainActivity.this.overridePendingTransition(
							R.anim.push_down_in, R.anim.push_down_out);
					break;
				case 3:
					try {
						Intent i = new Intent(Intent.ACTION_SEND);
						i.setType("text/plain");
						i.putExtra(Intent.EXTRA_SUBJECT, "GoodFood");
						String sAux = "\nLet me recommend you this application\n\n";
						sAux = sAux + "link";
						i.putExtra(Intent.EXTRA_TEXT, sAux);
						startActivity(Intent.createChooser(i, "choose one"));
					} catch (Exception e) { // e.toString();
					}

				}
				mDrawerLayout.closeDrawer(mDrawerList);

			}

		}
		mDrawerList.setOnItemClickListener(new SlideitemListener());

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		width1 = wm.getDefaultDisplay().getWidth();
		try
		{
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.setting, R.string.app_name, R.string.app_name) {
			public void onDrawerClosed(View view) {
				llv.setVisibility(View.VISIBLE);

				invalidateOptionsMenu();
				// mDrawerLayout.setDrawerLockMode(BIND_NOT_FOREGROUND);

			}
		

			public void onDrawerOpened(View drawerView) {

				try
				{
				mDrawerList.bringToFront();
				mDrawerLayout.requestLayout();
				llv.setVisibility(View.GONE);
				// ViewGroup.LayoutParams params =llv.getLayoutParams();
				// params.width = 0;
				// llv.setLayoutParams(params);
				// llv.requestLayout();
				invalidateOptionsMenu();
				}
				catch(NullPointerException e)
				{
					Toast.makeText(getApplicationContext(), "Wait while the app gets your location", Toast.LENGTH_LONG).show();
				}
			}
		};
		
		}catch(NullPointerException e)
		{					Toast.makeText(getApplicationContext(), "Wait while the app gets your location.FOr best results either reboot or go outside", Toast.LENGTH_LONG).show();
}

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		mDrawerList.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (scrollState == SCROLL_STATE_IDLE) {
					mDrawerList.bringToFront();
					mDrawerLayout.requestLayout();
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		});

		
		rootView=getWindow().getDecorView().findViewById(android.R.id.content);
//		adView=(AdView)rootView.findViewById(R.id.MyAdView);
//		 AdRequest adRequest=new AdRequest.Builder().build();
//		 
////			AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
////					 .addTestDevice("F0E3A57BED5A38D34F93EB7CD6873BC9")
////						.build();
//			 adView.loadAd(adRequest);
	}

	
	//*********************************INITIALISE ASYNC TASK************************************
	public void Initialise() {

		shareRegidTask = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				Log.d("dd", "background stuff");
				String result = jevan();

				return result;
			}

			@Override
			protected void onPostExecute(String result) {
				shareRegidTask = null;
				// llv.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

				pb.setVisibility(View.GONE);

				try {
					if (result.contains("ZERO_RESULTS")) {
						Toast.makeText(getApplicationContext(),
								"Sorry No results", Toast.LENGTH_LONG).show();

						layout.setBackgroundResource(R.drawable.sorry);

					}
				} catch (NullPointerException e) {

					layout.setBackgroundResource(R.drawable.sorry);
					Toast.makeText(getApplicationContext(),
							"Internet Problems Sorry No results",
							Toast.LENGTH_LONG).show();

				}

				adapter.notifyDataSetChanged();
				for (Places p : listplaces) {
					if (p.photoreference != null
							&& p.photoreference != "******") {

						im = new Imagedownload();
						imd.add(im);
						imd.get(kk).execute(p.photoreference, p.name);
						// Log.d("############","!!!!"+p.name+"-------"+p.photoreference);
						p.image = imd.get(kk).bitmap;
						kk++;

					}
					if (p.photoreference == "******"
							|| p.photoreference == null) {
						if (bitmap != null) {
							bitmap.recycle();
							bitmap = null;
						}
						p.image = BitmapFactory.decodeResource(getResources(),
								R.drawable.food);
					}

				}

			}

		};

	}

	//*********************************FETCH DATA USING GOOGLE PLACES API AND PARSE THE JSON DATA************************************
	
	public String jevan() {
		// requesturl="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference;=CoQBcgAAAJGU9w2tW6mWNM2EqE_mh0-CN5jeN2rpxu3ipHSj6RHApEUt0gTgRMaYYcNJd_5pWL1cvDDsy_p_NQXWPQFL8UZahrhMccQSJpw58d5rkzptNpMStHFGXIoUkNzk8-no612DpRhxXnal-TZK2F2D2Dn1NGH6YFCbziJ36ZwHJM6MEhCCixnd0fydmmgEo1s5nD6xGhTr3uNQU2PH63ORIn90QMd1Ix84Bw&key;="+googleAPIKey;

		// requesturl="https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&radius=500&key="+googleAPIKey1+"&types;=food&location=18.4947061,73.817773";
		// requesturl="https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJySg5JOu_wjsRuMy4AFw9GXM&key="+googleAPIKey;
		System.out.println("Request " + requesturl);// 18.495002, 73.820175
		// c9ae5f0d851e132783831c773c26c44e80bff39d

		client = createHttpClient();
		System.out.println("hello");

		req = new HttpPost(requesturl);
		System.out.println("hello");

		try {
			res = client.execute(req);
			StatusLine status = res.getStatusLine();
			int code = status.getStatusCode();
			System.out.println(code);
			if (code != 200) {
				System.out.println("Request Has not succeeded");
				finish();
			}
			 Log.d("$$$$$$$$",res.getEntity()+"");
			jsonentity = res.getEntity();
			in = jsonentity.getContent();

			results = convertStreamToString(in);

			jsonobj = new JSONObject(results);

			resarray = jsonobj.getJSONArray("results");

			if (resarray.length() == 0) {
			} else {
				int len = resarray.length();
				for (int j = 0; j < len; j++) {
					// Toast.makeText(getApplicationContext(),
					// resarray.getJSONObject(j).getString("name") ,
					// Toast.LENGTH_LONG).show();

					locs = resarray.getJSONObject(j).getJSONObject("geometry");
					locs = locs.getJSONObject("location");
					String lat = locs.getDouble("lat") + "";
					String lng = locs.getDouble("lng") + "";

					if (resarray.getJSONObject(j).has("opening_hours")) {
						openhrs = resarray.getJSONObject(j).getJSONObject(
								"opening_hours");

						opennow = openhrs.getString("open_now");
						if (opennow == "true") {
							opennow = "OPEN";
						} else {
							opennow = "CLOSED";

						}

					} else {
						opennow = "not available";
					}

					if (resarray.getJSONObject(j).has("photos")) {

						photos = resarray.getJSONObject(j).getJSONArray(
								"photos");
						photo_reference = photos.getJSONObject(0).getString(
								"photo_reference");
					} else {
						photo_reference = "******";
					}
					//
					if (resarray.getJSONObject(j).has("rating")) {
						rating = resarray.getJSONObject(j).getString("rating")
								+ "";
					} else {
						rating = 3.5 + "";
					}
					if (resarray.getJSONObject(j).has("formatted_address")) {
						addresst = resarray.getJSONObject(j).getString(
								"formatted_address");
					} else if (resarray.getJSONObject(j).has("vicinity")) {
						addresst = resarray.getJSONObject(j).getString(
								"vicinity");

					}
					plc = new Places(resarray.getJSONObject(j)
							.getString("name"), addresst, resarray
							.getJSONObject(j).getString("place_id"), lat, lng,
							rating, photo_reference, opennow);
					listplaces.add(plc);

				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();

		}

		return results;
	}

	private String convertStreamToString(InputStream in) {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		StringBuilder jsonstr = new StringBuilder();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				String t = line + "\n";
				jsonstr.append(t);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int maxLogSize = 1000;
		for (int i = 0; i <= jsonstr.toString().length() / maxLogSize; i++) {
			int start = i * maxLogSize;
			int end = (i + 1) * maxLogSize;
			end = end > jsonstr.toString().length() ? jsonstr.toString()
					.length() : end;
					// Log.d("%%%%%%%%%%%", jsonstr.toString().substring(start, end));
		}

		return jsonstr.toString();
	}

	
	//*********************************KNOW WHICH REQUEST IS TO BE SENT AND START ASYNC TASK************************************
	
	@Override
	public void onLocationChanged(Location loca) {
		
		Log.d("$$$$$$$$","psease come here quickl;y");
		// TODO Auto-generated method stub

		adapter = new CustomAdapter(this, listplaces);
		llv = (ListView) findViewById(R.id.lv1);
		llv.setAdapter(adapter);

		llv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> paramAnonymousAdapterView,
					View paramAnonymousView, final int paramAnonymousInt,
					long paramAnonymousLong) {

				Places ri = listplaces.get(paramAnonymousInt);
				String str = ri.name;
	Log.d("hmm", str + "ff");

				Intent i = new Intent(getApplicationContext(), Details.class);
				b = new Bundle();
				b.putString("id", ri.place_id);
				i.putExtras(b);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

			}
		});

		lat = loca.getLatitude();
		longi = loca.getLongitude();

		sharedPref = getSharedPreferences("places", 0);
		int i = sharedPref.getInt("change", 0);
		radius = sharedPref.getString("radius", "500");
		query = sharedPref.getString("query", "restaurant");
		String[] q = query.split(" ");
		query = "";
		for (int gg = 0; gg < q.length; gg++) {
			query = query + q[gg];
		}
		name = sharedPref.getString("name", "restaurant");

		if (name.contains(" ")) {
			String[] nameparts = name.split(" ");
			String namefinal = "";
			for (int ii = 0; ii < nameparts.length; ii++) {
				namefinal = namefinal + nameparts[ii];
			}
			name = namefinal;
		}

		keyword = sharedPref.getString("keyword", "restaurant");

		if (radius.contains("Within distance")) {
			radius = "2500";
		}

		if (i == 0) {
			Log.d("$$$$$$$", "query option 0----" + i);

			// requesturl="https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=500&key="+googleAPIKey1+"&types;=food&location="+lat+","+longi;
			requesturl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants&radius=500&key="
					+ googleAPIKey1
					+ "&types;=food&location="
					+ lat
					+ ","
					+ longi;
			Log.d("$$$$$$$$$$$", lat + "----" + longi);
		} else if (i == 1) {
			Log.d("$$$$$$$", "query option 1----" + i);

			requesturl = syntax + searchtype + "query=" + query + "&radius="
					+ radius + key + googleAPIKey1 + types + location + lat
					+ "," + longi;

			Log.d("@@@", requesturl + "--");
		} else if (i == 2) {
			Log.d("$$$$$$$", "query option 2----" + i);

			if (name == "**") {
				requesturl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&radius=500&key="
						+ googleAPIKey1
						+ "&types;=food&location="
						+ lat
						+ ","
						+ longi;
				Log.d("@@@", requesturl + "--");

			} else {
				requesturl = syntax + "nearbysearch/json?radius=" + radius
						+ key + googleAPIKey1 + types + "&keyword=" + keyword
						+ location + lat + "," + longi;
				Log.d("@@@", requesturl + "--");

			}
		} else if (i == 3) {
			Log.d("$$$$$$$", "query option 3----" + i);

			requesturl = syntax + searchtype + "query=" + query + "&radius="
					+ radius + key + googleAPIKey1 + types + location + lat
					+ "," + longi;

			Log.d("@@@", requesturl + "--");

		}
		e = sharedPref.edit();
		e.putInt("change", 0);
		e.commit();

		// Log.d("%%%%",alreadyrunningtask+"---"+shareRegidTask.getStatus());

		Initialise();
		shareRegidTask.execute(null, null, null);

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	
	//*********************************GET IMAGES OF EATERIES************************************

	public class Imagedownload extends AsyncTask<String, Integer, Bitmap> {
		Places plcs;
		String pref;
		Bitmap bitmap;
		MainActivity ma;
		int cnt = 0;

		// String rurl=
		// "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+pref+"&sensor=false&key=AIzaSyABknFwtqt5DdvLV9vxi6miEx6FrZmMA8k";

		@Override
		public Bitmap doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {

				String rurl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
						+ arg0[0] + "&sensor=false&key=" + googleAPIKey1;

				// Starting image download
				pref = arg0[0];
				 Log.d("asas","dfdfdf"+arg0[0]);
				bitmap = downloadImage(rurl);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return bitmap;
		}

		@Override
		public void onPostExecute(Bitmap result) {

			for (Places p : listplaces) {
				if (p.photoreference == pref) {
					p.image = result;
					// Log.d("!!!!!!!!!!!111",listplaces.get(cnt).name+"----"+listplaces.get(cnt).photoreference+"----"+pref);
					adapter.notifyDataSetChanged();

				}
			}
		}

		private Bitmap downloadImage(String strUrl) throws IOException {
			Bitmap bitmap = null;
			InputStream iStream = null;

			try {
				URL url = new URL(strUrl);

				/** Creating an http connection to communcate with url */
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();

				/** Connecting to url */
				urlConnection.connect();

				/** Reading data from url */
				iStream = urlConnection.getInputStream();

				/** Creating a bitmap from the stream returned from the url */
				
				bitmap = BitmapFactory.decodeStream(iStream);

			} catch (Exception e) {
				Log.d("Exception while downloading url", e.toString());
			} finally {
				iStream.close();
			}
			return bitmap;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	// @Override
	// protected void onPostCreate(Bundle savedInstanceState) {
	// super.onPostCreate(savedInstanceState);
	// // Sync the toggle state after onRestoreInstanceState has occurred.
	// mDrawerToggle.syncState();
	// }
	//
	// @Override
	// public void onConfigurationChanged(Configuration newConfig) {
	// super.onConfigurationChanged(newConfig);
	// // Pass any configuration change to the drawer toggls
	// mDrawerToggle.onConfigurationChanged(newConfig);
	// }
	//
	//

	public void onPause() {
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
		if (bitmap != null) {
			bitmap.recycle();
			bitmap = null;
			req.abort();
		}
		if (shareRegidTask != null) {
			shareRegidTask.cancel(true);
		}
		super.onPause();

	}

	public void onDestory() {
		if (bitmap != null) {
			bitmap.recycle();
			bitmap = null;
			overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
			req.abort();
		}
		if (shareRegidTask != null) {
			shareRegidTask.cancel(true);
		}
		super.onDestroy();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/**
	 * Handling intent data
	 */
	
	//*********************************HANDLE SEARCH WIDGET************************************
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query1 = intent.getStringExtra(SearchManager.QUERY);

			requesturl = syntax + searchtype + "query=" + query1 + "&radius="
					+ radius + key + googleAPIKey1 + types + location + lat
					+ "," + longi;
			Initialise();
			shareRegidTask.cancel(true);

			alreadyrunningtask = 1;
			Log.d("############", query1 + "---" + alreadyrunningtask);
			sharedPref = getSharedPreferences("places", 0);

			e = sharedPref.edit();
			e.putInt("change", 3);
			e.commit();
			e.putString("query", query1);
			e.commit();

		}

	}

	//
	//
	//*********************************HANDLE SSL CERTIFICATE PROBLEM************************************
	public static HttpClient createHttpClient() {
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params,
				HTTP.DEFAULT_CONTENT_CHARSET);
		HttpProtocolParams.setUseExpectContinue(params, true);

		SchemeRegistry schReg = new SchemeRegistry();
		schReg.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schReg.register(new Scheme("https",
				SSLSocketFactory.getSocketFactory(), 443));
		ClientConnectionManager conMgr = new ThreadSafeClientConnManager(
				params, schReg);

		return new DefaultHttpClient(conMgr, params);
	}

}