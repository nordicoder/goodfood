package com.example.places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Details extends Activity {
	ActionBar bar;
	AsyncTask<Void, Void, String> finddetails, post, getup;
	private String googleAPIKey = "AIzaSyABknFwtqt5DdvLV9vxi6miEx6FrZmMA8k";
	private String googleAPIKey1 = "AIzaSyC79Mt0Z02yLoFM04tMlieSFCfhwhHm2i0";
	private String googleAPIKey2 = "AIzaSyAPDWD8S9kqx4JP1DLSJKWzneZSUIpKPnU";
	HttpClient client;
	HttpResponse res;
	HttpPost req;
	InputStream in;
	JSONObject jsonobj;
	JSONObject resobj;
	String requesturl, ans = "";
	HttpEntity jsonentity;
	String pid;
	TextView address;
	TextView phoneno;
	TextView review;
	TextView website;
	TextView upvote;
	TextView hrstoday;
	SmsManager sms;
	Bundle b;
	Post pp;
	String results;
	Dialog dialog;
	JSONArray photoarray;
	String addresst, websitet, phonenot, reviewt, namet, intphoneno,
	upvotes = " ";
	List<String> photoreference = new ArrayList<String>();
	List<Bitmap> images = new ArrayList<Bitmap>();
	List<Imagedownload> imd = new ArrayList<Imagedownload>();
	Imagedownload im;
	ImageView imv;
	ImageButton rt, lft;
	ProgressBar pb;
	EditText guys, rdate;
	int cnt;
	ImageButton db1, db2;
	Context con;
	String openingtoday;
	String lat, lng;
	RelativeLayout layout;
	TextView descText;
	ImageButton show, hide;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		//*********************************INITIALISE************************************
		
		layout = (RelativeLayout) findViewById(R.id.rl);

		con = getApplicationContext();
		cnt = 0;
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		pb.setVisibility(View.VISIBLE);
		address = (TextView) (findViewById(R.id.address));
		phoneno = (TextView) (findViewById(R.id.phoneno));
		// review=(TextView)(findViewById(R.id.review));
		website = (TextView) (findViewById(R.id.website));
		upvote = (TextView) (findViewById(R.id.upvote));
		hrstoday = (TextView) (findViewById(R.id.hourstoday));

		// review.setMovementMethod(new ScrollingMovementMethod());
		imv = (ImageView) (findViewById(R.id.imageView1));
		lft = (ImageButton) (findViewById(R.id.imageButton1));

		rt = (ImageButton) (findViewById(R.id.imageButton2));

		lft.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (cnt > 0 && images.size() != 0) {
					cnt--;
					imv.setImageBitmap(images.get(cnt));

				}
			}
		});

		rt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (cnt < images.size() - 1 && images.size() != 0) {
					cnt++;
					imv.setImageBitmap(images.get(cnt));
				}

			}
		});

		descText = (TextView) findViewById(R.id.description_text);
	
		descText.setMaxLines(Integer.MAX_VALUE);
		

		bar = getActionBar();
		// String concat=
		// String [] aray=concat.split("//");
		pid = getIntent().getExtras().getString("id");
		bar.setSplitBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#A00000")));
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
		bar.hide();
		// bar.setTitle(pid);
		getResources();
		imv.setImageBitmap((BitmapFactory.decodeResource(getResources(),
				R.drawable.foods)));
		//*********************************ASYNC TASK TO GET DETAILS OF THE PLACE************************************
		finddetails = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... arg0) {
				// TODO Auto-generated method stub
				String result = jevan();
				return result;
			}

			protected void onPostExecute(String result) {

				try {
					if (result.contains("ZERO_RESULTS")) {
						Toast.makeText(getApplicationContext(),
								"Sorry No results", Toast.LENGTH_LONG).show();

						layout.setBackgroundResource(R.drawable.sorry);

					}
				} catch (NullPointerException e) {

					layout.setBackgroundResource(R.drawable.sorry);
					Toast.makeText(getApplicationContext(),
							"Internet Problems Sorry No results",
							Toast.LENGTH_LONG).show();

				}

				pb.setVisibility(View.GONE);
				bar.show();

				bar.setTitle(namet);
				descText.setText(reviewt);
				address.setText(addresst);
				phoneno.setText("\t                       " + phonenot);
				website.setText("\t                       " + websitet);
				hrstoday.setText(openingtoday);

				website.setClickable(true);
				website.setMovementMethod(LinkMovementMethod.getInstance());
				String text = "<a href='" + websitet + "'" + ">" + websitet
						+ " </a>";
				website.setText(Html.fromHtml(text));

				for (int jj = 0; jj < photoreference.size(); jj++) {
					im = new Imagedownload();
					imd.add(im);
					if (photoreference.get(jj) != ""
							|| photoreference.get(jj) != null) {
						imd.get(jj).execute(photoreference.get(jj));
						Log.d("############", "!!!!");
						// images.add(imd.get(jj).bitmap);
					} else {
						images.add(BitmapFactory.decodeResource(getResources(),
								R.drawable.foods));
						imv.setImageBitmap((BitmapFactory.decodeResource(
								getResources(), R.drawable.foods)));
					}
				}
				try {
					getup = new AsyncTask<Void, Void, String>() {

						@Override
						protected String doInBackground(Void... arg0) {
							// TODO Auto-generated method stub
							String namse = "";
							try {
								if (namet.contains(" ")) {
									String[] namar = namet.split(" ");
									namse = "";
									for (int yy = 0; yy < namar.length; yy++) {
										namse = namse + namar[yy];
									}
								} else {
									namse = namet;
								}
							} catch (NullPointerException e) {

							}
							pp = new Post();
							upvotes = pp.get(namse + lat + lng);
							return upvotes;
						}

						protected void onPostExecute(String result) {
							upvote.setText(upvotes + "+");

						}
					};
					getup.execute();
				} catch (NullPointerException e) {
					layout.setBackgroundResource(R.drawable.sorry);
					Toast.makeText(getApplicationContext(),
							"Internet Problems Sorry No results",
							Toast.LENGTH_LONG).show();

				}

			}

		};
		finddetails.execute();

		// imv.setImageBitmap(images.get(1));

	}

	@Override
	public Resources getResources() {
		return new ResourceFix(super.getResources());
	}

	private class ResourceFix extends Resources {
		private int targetId = 0;

		ResourceFix(Resources resources) {
			super(resources.getAssets(), resources.getDisplayMetrics(),
					resources.getConfiguration());
			targetId = Resources.getSystem().getIdentifier(
					"split_action_bar_is_narrow", "bool", "android");
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean getBoolean(int id) throws Resources.NotFoundException {
			return targetId == id || super.getBoolean(id);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actions, menu);
		return true;
	}

	//*********************************HANDLE ACTIONBAR EVENTS************************************
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		switch (id) {
		case R.id.upvote:
			Log.d("@@@@@@@@@@", "1");

			post = new AsyncTask<Void, Void, String>() {

				@Override
				protected String doInBackground(Void... arg0) {
					// TODO Auto-generated method stub

					String email = "";
					Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level
					// 8+
					Account[] accounts = AccountManager.get(
							getApplicationContext()).getAccounts();
					for (Account account : accounts) {
						if (emailPattern.matcher(account.name).matches()) {
							email = account.name;
							break;
						}
					}

					String[] namar = namet.split(" ");
					String namse = "";
					for (int yy = 0; yy < namar.length; yy++) {
						namse = namse + namar[yy];
					}

					pp = new Post();
					pp.post(namse + lat + lng + "***" + email);
					return null;
				}

				protected void onPostExecute(String result) {
				}
			};
			post.execute();

			Toast.makeText(getApplicationContext(), "Upvoted",
					Toast.LENGTH_SHORT).show();

			break;
		case R.id.map:
			Intent i = new Intent(getApplicationContext(), Map.class);
			b = new Bundle();
			b.putString("id", lat + "//" + lng);
			i.putExtras(b);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			con.startActivity(i);

			break;
		case R.id.Call:
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + phonenot));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			con.startActivity(intent);
			Log.d("@@@@@@@@@@", "3");

			break;
		case R.id.Recommend:

			dialog = new Dialog(Details.this);
			dialog.setContentView(R.layout.custom1);
			dialog.setTitle("Recommend to a friend");

			db1 = (ImageButton) dialog.findViewById(R.id.imageButton1);
			db2 = (ImageButton) dialog.findViewById(R.id.imageButton2);

			// if button is clicked, close the custom dialog
			db1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					guys = (EditText) dialog.findViewById(R.id.editText1);
					if (guys.getText().toString() != null) {
						String msg = "Hey !! "
								+ namet
								+ " is Great!! You should definitely visit this place at "
								+ addresst
								+ ".  Sending this via GoodFood a must have app developed by Omkar Patil!";

						Intent mail = new Intent(Intent.ACTION_SENDTO, Uri
								.fromParts("mailto", guys.getText().toString(),
										null));
						mail.putExtra(Intent.EXTRA_SUBJECT, "Great Place!");
						mail.putExtra(Intent.EXTRA_TEXT, msg);

						try {
							startActivity(Intent.createChooser(mail,
									"Send mail..."));
						} catch (android.content.ActivityNotFoundException ex) {
							Toast.makeText(Details.this,
									"There are no email clients installed.",
									Toast.LENGTH_SHORT).show();
						}

						dialog.dismiss();
					}

				}
			});

			db2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					dialog.dismiss();

				}
			});

			dialog.show();

			Log.d("@@@@@@@@@@", "4");

			break;
		case R.id.Reserve:
			Log.d("@@@@@@@@@@", "5");
			sms = SmsManager.getDefault();
			dialog = new Dialog(Details.this);
			dialog.setContentView(R.layout.custom);
			dialog.setTitle("Reserve Today!");

			db1 = (ImageButton) dialog.findViewById(R.id.imageButton1);
			db2 = (ImageButton) dialog.findViewById(R.id.imageButton2);

			// if button is clicked, close the custom dialog
			db1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					guys = (EditText) dialog.findViewById(R.id.editText1);
					rdate = (EditText) dialog.findViewById(R.id.editText2);
					if (guys.getText().toString() != null
							&& rdate.getText().toString() != null) {
						String msg = "Please Reserve a table for "
								+ guys.getText().toString() + " at "
								+ rdate.getText().toString();
						sms.sendTextMessage(intphoneno, null, msg, null, null);
						dialog.dismiss();
					}

				}
			});

			db2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					dialog.dismiss();

				}
			});

			dialog.show();

			break;

		}
		return super.onOptionsItemSelected(item);
	}
	//*********************************FETCH DATA FROM GOOGLE PLACES API AND PARSE JSON DATA************************************
	private String jevan() {
		// requesturl="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference;=CoQBcgAAAJGU9w2tW6mWNM2EqE_mh0-CN5jeN2rpxu3ipHSj6RHApEUt0gTgRMaYYcNJd_5pWL1cvDDsy_p_NQXWPQFL8UZahrhMccQSJpw58d5rkzptNpMStHFGXIoUkNzk8-no612DpRhxXnal-TZK2F2D2Dn1NGH6YFCbziJ36ZwHJM6MEhCCixnd0fydmmgEo1s5nD6xGhTr3uNQU2PH63ORIn90QMd1Ix84Bw&key;="+googleAPIKey;

		// requesturl="https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&radius=500&key="+googleAPIKey+"&types;=food&location=18.495002,73.820175";
		requesturl = "https://maps.googleapis.com/maps/api/place/details/json?placeid="
				+ pid + "&key=" + googleAPIKey1;
		System.out.println("Request " + requesturl);// 18.495002, 73.820175
		// c9ae5f0d851e132783831c773c26c44e80bff39d
		client = createHttpClient();
		System.out.println("hello");

		req = new HttpPost(requesturl);
		System.out.println("hello");

		try {
			res = client.execute(req);
			StatusLine status = res.getStatusLine();
			int code = status.getStatusCode();
			System.out.println(code);
			if (code != 200) {
				System.out.println("Request Has not succeeded");
				finish();
			}
			// Log.d("$$$$$$$$",res.getEntity()+"");
			jsonentity = res.getEntity();
			in = jsonentity.getContent();

			results = convertStreamToString(in);

			jsonobj = new JSONObject(results);

			resobj = jsonobj.getJSONObject("result");

			if (resobj.length() == 0) {
			} else {

				lat = (resobj.getJSONObject("geometry")).getJSONObject(
						"location").getString("lat");
				lng = (resobj.getJSONObject("geometry")).getJSONObject(
						"location").getString("lng");

				if (resobj.has("formatted_address")) {
					addresst = (resobj.getString("formatted_address"));

				} else {
					addresst = "Not available";
				}

				if (resobj.has("formatted_phone_number")) {
					phonenot = (resobj.getString("formatted_phone_number"));

				} else {
					phonenot = "Not available";
				}
				if (resobj.has("international_phone_number")) {
					intphoneno = (resobj
							.getString("international_phone_number"));

				} else {
					intphoneno = "Not available";
				}

				if (resobj.has("website")) {
					websitet = (resobj.getString("website"));
				} else {
					websitet = "Not available";

				}
				if (resobj.has("reviews")) {
					reviewt = "";
					for (int kk = 0; kk < resobj.getJSONArray("reviews")
							.length(); kk++) {
						reviewt = reviewt
								+ "\n"
								+ (resobj.getJSONArray("reviews")
										.getJSONObject(kk)
										.getString("author_name")
										+ "\n"
										+ (resobj.getJSONArray("reviews")
												.getJSONObject(kk)
												.getString("text"))
												+ "\n"
												+ "Rating :- " + (resobj.getJSONArray(
														"reviews").getJSONObject(kk)
														.getString("rating")));
					}
				} else {
					reviewt = ("Not available");
				}
				if (resobj.has("name")) {
					namet = (resobj.getString("name"));
				} else {
					namet = "Not available";

				}
				if (resobj.has("photos")) {
					photoarray = resobj.getJSONArray("photos");
					for (int ii = 0; ii < photoarray.length(); ii++) {
						photoreference.add(photoarray.getJSONObject(ii)
								.getString("photo_reference"));
					}
				} else {
					images.add(BitmapFactory.decodeResource(getResources(),
							R.drawable.foods));
				}
				if (resobj.has("opening_hours")) {
					if (resobj.getJSONObject("opening_hours").has(
							"weekday_text")) {
						Calendar calendar = Calendar.getInstance();
						int day = calendar.get(Calendar.DAY_OF_WEEK);

						if (day == 1) {
							openingtoday = resobj
									.getJSONObject("opening_hours")
									.getJSONArray("weekday_text").getString(6);
						} else {
							openingtoday = resobj
									.getJSONObject("opening_hours")
									.getJSONArray("weekday_text")
									.getString(day - 2);
						}

					}
				}

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();

		}

		return results;
	}

	private String convertStreamToString(InputStream in) {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		StringBuilder jsonstr = new StringBuilder();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				String t = line + "\n";
				jsonstr.append(t);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		int maxLogSize = 1000;
		for (int i = 0; i <= jsonstr.toString().length() / maxLogSize; i++) {
			int start = i * maxLogSize;
			int end = (i + 1) * maxLogSize;
			end = end > jsonstr.toString().length() ? jsonstr.toString()
					.length() : end;
					Log.d("%%%%%%%%%%%", jsonstr.toString().substring(start, end));
		}

		// Log.d("asas",jsonstr.toString());
		return jsonstr.toString();
	}
	//*********************************GET ALL IMAGES AVAILABLE FOR A PLACE************************************
	public class Imagedownload extends AsyncTask<String, Integer, Bitmap> {
		Places plcs;
		String pref;
		Bitmap bitmap;
		MainActivity ma;
		int cnt = 0;

		// String rurl=
		// "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+pref+"&sensor=false&key=AIzaSyABknFwtqt5DdvLV9vxi6miEx6FrZmMA8k";

		@Override
		public Bitmap doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {

				String rurl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="
						+ arg0[0] + "&sensor=false&key=" + googleAPIKey1;

				// Starting image download
				pref = arg0[0];
				// Log.d("asas","dfdfdf"+arg0[0]);
				bitmap = downloadImage(rurl);
				// Log.d("############","ahemammm");
				images.add(bitmap);

			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return bitmap;
		}

		@Override
		public void onPostExecute(Bitmap result) {
			imv.setImageBitmap(images.get(cnt));

			Log.d("############", "ahemammm");

			// for(Places p:listplaces)
			// {
			// if(p.photoreference==pref)
			// {
			// p.image=result;
			// //Log.d("!!!!!!!!!!!111",listplaces.get(cnt).name+"----"+listplaces.get(cnt).photoreference+"----"+pref);
			// adapter.notifyDataSetChanged();
			//
			// }
			// }
		}

		private Bitmap downloadImage(String strUrl) throws IOException {
			Bitmap bitmap = null;
			InputStream iStream = null;

			try {
				URL url = new URL(strUrl);

				/** Creating an http connection to communcate with url */
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();

				/** Connecting to url */
				urlConnection.connect();

				/** Reading data from url */
				iStream = urlConnection.getInputStream();

				/** Creating a bitmap from the stream returned from the url */
				bitmap = BitmapFactory.decodeStream(iStream);

			} catch (Exception e) {
				Log.d("Exception while downloading url", e.toString());
			} finally {
				iStream.close();
			}
			Log.d("%%%%%%%%%%", "yo");
			return bitmap;
		}

	}
	//*********************************HANDLE SSL CERTIFICATE PROBLEM************************************
	public static HttpClient createHttpClient() {
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params,
				HTTP.DEFAULT_CONTENT_CHARSET);
		HttpProtocolParams.setUseExpectContinue(params, true);

		SchemeRegistry schReg = new SchemeRegistry();
		schReg.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schReg.register(new Scheme("https",
				SSLSocketFactory.getSocketFactory(), 443));
		ClientConnectionManager conMgr = new ThreadSafeClientConnManager(
				params, schReg);

		return new DefaultHttpClient(conMgr, params);
	}

}
