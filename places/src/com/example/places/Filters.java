package com.example.places;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Filters extends Activity {
	RadioGroup radiocat;
	EditText ed1, ed2;
	List<String> types = new ArrayList<String>();
	List<String> distance = new ArrayList<String>();
	ImageButton ib1;
	SharedPreferences sharedpref;
	Editor e;
	Intent set;
	int opt = 1;
	ActionBar bar;
	Spinner s1, s2;
	AdView adView;

	View rootView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filters);
		sharedpref = getSharedPreferences("places", 0);
		e = sharedpref.edit();

		//*********************************INITIALISE************************************
		
		bar = getActionBar();

		bar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#FF000000")));
		bar.setTitle("Filters");

		ed1 = (EditText) findViewById(R.id.editText1);
		ed2 = (EditText) findViewById(R.id.editText2);
		s1 = (Spinner) findViewById(R.id.spinner);
		s2 = (Spinner) findViewById(R.id.spinner1);

		ib1 = (ImageButton) findViewById(R.id.imageButton1);

		ed2.setText("Name! eg:- PizzaHut");
		ed2.setVisibility(View.GONE);
		s1.setVisibility(View.GONE);

		radiocat = (RadioGroup) findViewById(R.id.radioOption);

		radiocat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				Log.d("$$$$$$", checkedId + "----");
				if (checkedId % 2 != 0) {
					ed1.setVisibility(View.VISIBLE);
					ed2.setVisibility(View.GONE);
					s1.setVisibility(View.GONE);

					e.putInt("change", 1);
					opt = 1;
					e.commit();
				} else {
					ed1.setVisibility(View.GONE);
					// ed2.setVisibility(View.VISIBLE);
					s1.setVisibility(View.VISIBLE);
					e.putInt("change", 2);
					opt = 2;
					e.commit();

				}
			}
		});

		Spinner spinner = (Spinner) findViewById(R.id.spinner);

		types.add("Pizza");
		types.add("Burger");
		types.add("cafe");
		types.add("Chinese");
		types.add("Indian");
		types.add("NonVeg");
		types.add("Chicken");
		types.add("Bar");
		types.add("Bakery");
		types.add("Indian");
		types.add("European");

		distance.add("Within distance of(in meters)?");

		distance.add("500");
		distance.add("1000");
		distance.add("1500");
		distance.add("2000");
		distance.add("2500");
		distance.add("3000");
		distance.add("3500");
		distance.add("4000");
		distance.add("4500");
		distance.add("5000");
		distance.add("7000");
		distance.add("10000");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				R.layout.spinner_item, types);
		dataAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(dataAdapter);
		s1.getBackground().setColorFilter(
				getResources().getColor(R.color.White),
				PorterDuff.Mode.SRC_ATOP);

		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
				R.layout.spinner_item, distance);
		dataAdapter1
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(dataAdapter1);
		s2.getBackground().setColorFilter(
				getResources().getColor(R.color.White),
				PorterDuff.Mode.SRC_ATOP);

		s1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				Log.d("$$$$$$$$", parent.getItemAtPosition(pos).toString()
						+ "---");
				e.putString("keyword", parent.getItemAtPosition(pos).toString());
				e.commit();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		s2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View arg1,
					int pos, long arg3) {
				// TODO Auto-generated method stub
				Log.d("$$$$$$$$", parent.getItemAtPosition(pos).toString()
						+ "---");
				e.putString("radius", parent.getItemAtPosition(pos).toString());
				e.commit();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		set = new Intent(getApplicationContext(), MainActivity.class);
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
		set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		//*********************************SEND TYPE OF SEARCH REQUEST TO THE MAINACTIVITY WHERER THE SEARCH WILL BE EXECUTED************************************
		
		
		ib1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (opt == 1) {
					if (ed1.getText().toString() != "Query! eg:- restaurants in pune"
							&& ed1.getText().toString() != "") {
						e.putString("query", ed1.getText().toString());
						Log.d("$$$$$$$", "query" + ed1.getText().toString());
						e.commit();

						e.putInt("change", 1);
						e.commit();

						getApplicationContext().startActivity(set);

					} else {
						Toast.makeText(getApplicationContext(),
								"Enter a valid Query!", Toast.LENGTH_LONG)
								.show();
					}
				} else if (opt == 2) {
					if (ed2.getText().toString() != "Name! eg:- PizzaHut"
							&& ed2.getText().toString() != "") {
						e.putString("name", ed2.getText().toString());
						e.commit();

						e.putInt("change", 2);
						e.commit();
						getApplicationContext().startActivity(set);

					} else {
						e.putString("name", "**");
						e.commit();
						getApplicationContext().startActivity(set);

						// Toast.makeText(getApplicationContext(),
						// "Enter a valid Query!", Toast.LENGTH_LONG).show();
					}
				}
			}
		});
		rootView=getWindow().getDecorView().findViewById(android.R.id.content);
//		adView=(AdView)rootView.findViewById(R.id.MyAdView);
//		 AdRequest adRequest=new AdRequest.Builder().build();
//		 
////			AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
////					 .addTestDevice("F0E3A57BED5A38D34F93EB7CD6873BC9")
////						.build();
//			 adView.loadAd(adRequest);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.filters, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onPause() {
		overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);

		super.onPause();

	}
}
