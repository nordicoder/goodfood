package com.example.places;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;

public class Welcome extends Activity {
	ImageButton setting, help, nearby;
	FrameLayout fl1, fl2, fl3;
	ActionBar bar;
	Dialog dialog;
	ImageButton db1, db2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

		bar = getActionBar();

		bar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#FF000000")));
		bar.setTitle("Welcome");

		fl1 = (FrameLayout) findViewById(R.id.frameLayout1);
		fl2 = (FrameLayout) findViewById(R.id.frameLayout2);

		fl3 = (FrameLayout) findViewById(R.id.frameLayout3);

		setting = (ImageButton) findViewById(R.id.button1);
		nearby = (ImageButton) findViewById(R.id.button2);

		help = (ImageButton) findViewById(R.id.button3);

		Animation slided = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.zoom_in);
		Animation slideu = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.zoom_in);
		Animation slideb = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.zoom_in);

		fl1.setAnimation(slided);
		fl1.startAnimation(slided);

		fl2.setAnimation(slideu);
		fl2.startAnimation(slideu);

		fl3.startAnimation(slideb);
		fl3.startAnimation(slideb);

		setting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent set = new Intent(getApplicationContext(), Filters.class);
				set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(set);
				Welcome.this.overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);

			}
		});

		nearby.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent set = new Intent(getApplicationContext(),
						MainActivity.class);
				set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(set);
				Welcome.this.overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);

			}
		});
		help.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// // TODO Auto-generated method stub
			
				Intent set = new Intent(getApplicationContext(),
						HelpActivity.class);
				set.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(set);
				Welcome.this.overridePendingTransition(R.anim.push_down_in,
						R.anim.push_down_out);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
